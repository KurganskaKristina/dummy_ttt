from setuptools import setup

setup(
    name="dummy_ttt",
    packages=["dummy_ttt"],
    version='0.1',
    license='MIT',        # Chose a license from here: https://help.github.com/articles/licensing-a-repository
    description='Small dummy package',
    author='Kristina',
    author_email="test@test.com",
    url='https://github.com/user/reponame',
    download_url='https://github.com/user/reponame/archive/v_01.tar.gz',
    keywords=['SOME', 'MEANINGFULL', 'KEYWORDS'],
    include_package_data=True,
)