# dummy_ttt
Can be installed with: `pip install dummy-ttt==0.1`

# how to upload package to PYPI

1. add setup.py, setup.cfg, LICENSE.txt, and README.md(optional) files
2. cd "C://PATH//TO//YOUR//FOLDER"
3. python setup.py sdist
4. pip install twine
5. twine upload dist/*

now you can install package from PYPI: `pip install YOURPACKAGENAME`